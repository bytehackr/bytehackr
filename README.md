<h1 align="center">Hi 👋, I'm Sandipan Roy</h1>
<h3 align="center">A passionate Security Engineer from India</h3>


<p style='text-align: justify;'> An independent and self-motivated computer science graduate with 2 years of experience in Information Security. Completed M.Sc. in Computer Science with a demonstrated history of working in Web/Infra Security, Application Security & Vulnerability Management. Also having experience in penetration testing and reporting and provided a powerful combination of analysis, implementation, and customer support. I’m truly passionate about my work and always increase my knowledge. I also like to try new technologies and improve myself under each point of view.</p>

<a href="https://t.me/bytehackr/" target="_blank"><img src="https://cdn2.iconfinder.com/data/icons/social-media-2199/64/social_media_isometric_19-telegram-512.png" height="100px" width="100px" alt="Telegram" align="right"></a>
<a href="https://facebook.com/bytehackr" target="_blank"><img src="https://cdn2.iconfinder.com/data/icons/social-media-2199/64/social_media_isometric_1-facebook-512.png" height="100px" width="100px" alt="Facebook" align="right"></a>
<a href="https://twitter.com/bytehackr" target="_blank"><img src="https://cdn2.iconfinder.com/data/icons/social-media-2199/64/social_media_isometric_6-twitter-512.png" height="100px" width="100px" alt="Twitter" align="right"></a><a href="https://www.linkedin.com/in/bytehackr/" target="_blank"><img src="https://cdn2.iconfinder.com/data/icons/social-media-2199/64/social_media_isometric_14-linkedin-512.png" height="100px" width="100px" alt="Twitter" align="right"></a>
Skills: Penetration Test | Threat Hunting | Security Research

- 🔭 I’m currently working at [Red Hat](https://redhat.com/) as Associate Product Security Engineer.

- 📫 Reach me anytime at Linkedin.





